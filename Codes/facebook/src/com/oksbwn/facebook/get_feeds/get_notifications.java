package com.oksbwn.facebook.get_feeds;

import java.text.SimpleDateFormat;
import java.util.Date;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.Notification;
import facebook4j.ResponseList;

public class get_notifications {
	private ResponseList<Notification> notificationFb;
	private Notification[] singleNotifications;
	private int notificationsSize;
	private SimpleDateFormat sdfDate= new SimpleDateFormat("dd/MMM/yyyy");
	private SimpleDateFormat sdfTime= new SimpleDateFormat("HH:mm");
	public get_notifications(Facebook FbInstance){
		try {
			notificationFb=FbInstance.getNotifications(false);
			notificationsSize=notificationFb.size();
			singleNotifications=new Notification[notificationsSize];

			for(int i=0;i<notificationsSize;i++){
					singleNotifications[i]=notificationFb.get(i);
				}
			} catch (FacebookException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int getSize(){
		return this.notificationsSize;
	}
	public String getTitle(int index){
		return this.singleNotifications[index].getTitle();
	}
	public String getFrom(int index){
		return this.singleNotifications[index].getFrom().getName();
	}
	public String getDate(int index){
		Date dat= singleNotifications[index].getUpdatedTime();

		return this.sdfDate.format(dat);
	}
	public String getTime(int index){
		Date dat= singleNotifications[index].getUpdatedTime();

		return this.sdfTime.format(dat);
	}
	public String getID(int index){
		return this.singleNotifications[index].getId();
	}
}
